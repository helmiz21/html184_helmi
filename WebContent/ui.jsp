<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="ISO-8859-1">
    <title>Penerimaan UI</title>
</head>

<body>
    <h3>Buat Account Baru</h3>
    For international applicants, please contact our International Office
    regarding application procedure. This registration form is meant for
    Indonesian citizen only. Harap menggunakan account lama Anda jika Anda
    telah membuat pendaftaran sebelumnya.
    <form action="#">
        <table>

            <tr>
                <td>
                    <H4>Login</H4>
                </td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td style="color: red;display: none" id="usernameWarning1">Field ini harus diisi</td>
            </tr>


            <tr>
                <td>Username</td>
                <td><input type="text" id="username"></td>
            </tr>

            <tr>
            </tr>

            <tr>
                <td></td>
                <td>Username yang Anda inginkan (hanya dapat terdiri dari
                    huruf, angka, dan _). Username tidak boleh mengandung spasi.</td>
            </tr>
            <tr>
                <td></td>
                <td style="color: red;display: none" id="passwordWarning1"></td>
            </tr>

            <tr>
                <td>Password</td>
                <td><input type="password" id="password"></td>
            </tr>

            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>Masukkan password. Password minimal terdiri dari 6 karakter
                    dan case sensitive (A beda dengan a).</td>
            </tr>
            <tr>
                <td></td>
                <td style="color: red;display: none" id="repasswordWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Ulangi password</td>
                <td><input type="password" id="repassword"></td>
            </tr>

            <tr>
                <td></td>
            </tr>

            <!-----------------------------------------------IDENTITAS---------------------------------------->
            <!------------------- -------------------------------------------------------------------------- -->

            <tr>
                <td>
                    <H4>Identitas</H4>
                </td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td style="color: red;display: none" id="namezWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Nama sesuai identitas * </td>
                <td><input type="text" id="namez"></td>
            </tr>

            <tr>
                <td></td>
                <td>Nama sesuai yang tertulis di kartu identitas, tanpa
                    gelar akademis/sertifikasi.</td>
            </tr>

            <tr>
                <td></td>
                <td>Nama diawali huruf besar di awal dan setelah spasi,
                    contoh benar: Abc Def, contoh salah: abc def, ABC DEF.</td>
            </tr>

            <!-------------------------------------------------------------------->
            <tr>
                <td></td>
                <td style="color: red;display: none" id="ijazahWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Nama sesuai ijazah * </td>
                <td><input type="text" id="ijazah"></td>
            </tr>

            <tr>
                <td></td>
                <td>Nama sesuai yang tertulis di kartu identitas, tanpa
                    gelar akademis/sertifikasi.</td>
            </tr>

            <tr>
                <td></td>
                <td>Nama diawali huruf besar di awal dan setelah spasi,
                    contoh benar: Abc Def, contoh salah: abc def, ABC DEF.</td>
            </tr>
            <!-------------------------------------------------------------------->
            <tr>
                <td></td>
                <td style="color: red;display: none" id="identWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Jenis identitas * </td>
                <td><select id="ident" onclick="pilihIdent();">
                        <option value="">-- Pilih --</option>
                        <option value="ktp">KTP</option>
                        <option value="sim">SIM</option>
                        <option value="paspor">Paspor</option>
                        <option value="karpel">Kartu Pelajar(Berfoto)</option>
                    </select></td>
            </tr>

            <tr>
                <td></td>
                <td>Pilih jenis identitas Anda. Identitas harus dibawa ketika ujian/daftar ulang.</td>
            </tr>

            <tr>
                <td></td>
                <td style="color: red;display: none" id="noIdentWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Nomor identitas * </td>
                <td><input type="text" id="noIdent"></td>
            </tr>
            <!-------------------------------------------------------------------->

            <tr>
                <td>Kewarganegaraan * </td>
                <td><select id="kewagg" onclick="pilihKewagg();">
                        <option value="indonesia">Indonesia</option>

                    </select></td>
            </tr>
            <!-------------------------------------------------------------------->
            <tr>
                <td></td>
                <td style="color: red;display: none" id="jkWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Jenis kelamin * </td>
                <td>
                    <input type="radio" name="jk" value="pria">Pria<br>
                    <input type="radio" name="jk" value="wanita">Wanita<br>
                </td>
            </tr>
            <!-------------------------------------------------------------------->
            <tr>
                <td></td>
                <td style="color: red;display: none" id="tglWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Tanggal Lahir * </td>
                <td><select id="hari">
                        <option value="">Tgl</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select>
                    <select id="bulan">
                        <option value="">Bln</option>
                        <option value="1">Jan</option>
                        <option value="2">Feb</option>
                        <option value="3">Mar</option>
                        <option value="4">Apr</option>
                        <option value="5">Mei</option>
                        <option value="6">Jun</option>
                        <option value="7">Jul</option>
                        <option value="8">Agu</option>
                        <option value="9">Sep</option>
                        <option value="10">Okt</option>
                        <option value="11">Nov</option>
                        <option value="12">Des</option>
                    </select>
                    <select id="tahun">
                        <option value="">Tahun</option>
                        <option value="2015">2015</option>
                        <option value="2014">2014</option>
                        <option value="2013">2013</option>
                        <option value="2012">2012</option>
                        <option value="2011">2011</option>
                        <option value="2010">2010</option>
                        <option value="2009">2009</option>
                        <option value="2008">2008</option>
                        <option value="2007">2007</option>
                        <option value="2006">2006</option>
                        <option value="2005">2005</option>
                        <option value="2004">2004</option>
                        <option value="2003">2003</option>
                        <option value="2002">2002</option>
                        <option value="2001">2001</option>
                        <option value="2000">2000</option>
                        <option value="1999">1999</option>
                        <option value="1998">1998</option>
                        <option value="1997">1997</option>
                        <option value="1996">1996</option>
                        <option value="1995">1995</option>
                        <option value="1994">1994</option>
                        <option value="1993">1993</option>
                        <option value="1992">1992</option>
                        <option value="1991">1991</option>
                        <option value="1990">1990</option>
                        <option value="1989">1989</option>
                        <option value="1988">1988</option>
                        <option value="1987">1987</option>
                        <option value="1986">1986</option>
                        <option value="1985">1985</option>
                        <option value="1984">1984</option>
                        <option value="1983">1983</option>
                        <option value="1982">1982</option>
                        <option value="1981">1981</option>
                        <option value="1980">1980</option>
                        <option value="1979">1979</option>
                        <option value="1978">1978</option>
                        <option value="1977">1977</option>
                        <option value="1976">1976</option>
                        <option value="1975">1975</option>
                        <option value="1974">1974</option>
                        <option value="1973">1973</option>
                        <option value="1972">1972</option>
                        <option value="1971">1971</option>
                        <option value="1970">1970</option>
                    </select>

                </td>
            </tr>
            <!-----------------------------------------------KONTAK-------------------------------------------->
            <!------------------- -------------------------------------------------------------------------- -->
            <tr>
                <td>
                    <H4>Kontak</H4>
                </td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td style="color: red;display: none" id="alamat1Warning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Alamat tetap * </td>
                <td><input type="text" id="alamat1"></td>
            </tr>
            <!-------------------------------------------------------------------->

            <tr>
                <td></td>
                <td style="color: red;display: none" id="negaraWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Negara * </td>
                <td><select id="negara" onclick="pilihPropinsi()">
                        <option value="">-- Pilih --</option>
                        <option value="india">India</option>
                        <option value="indonesia" selected>Indonesia</option>
                        <option value="united_kingdom">United Kingdom</option>
                    </select></td>
            </tr>
            <!-------------------------------------------------------------------->

            <tr>
                <td></td>
                <td style="color: red;display: none" id="propinsiWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Propinsi * </td>
                <td><select id="propinsi" onclick="pilihKk();">
                        <option value="">-- Pilih --</option>
                        <option value="banten">Banten</option>
                        <option value="jakarta">DKI Jakarta</option>
                        <option value="jawa_barat">Jawa Barat</option>
                    </select></td>
            </tr>
            <!-------------------------------------------------------------------->

            <tr>
                <td></td>
                <td style="color: red;display: none" id="kkWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Kabupaten/Kotamadya * </td>
                <td><select id="kk">
                    <option value=""></option>
                    </select></td>
            </tr>
            <!-------------------------------------------------------------------->

            <tr>
                <td></td>
                <td style="color: red;display: none" id="alamat2Warning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Alamat saat ini * </td>
                <td><input type="text" id="alamat2"></td>
            </tr>
            <!-------------------------------------------------------------------->

            <tr>
                <td></td>
                <td style="color: red;display: none" id="telpWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>No. Telepon * </td>
                <td><input type="text" id="telp" required pattern="[^a-zA-Z!@#$%^&*-+=_~`'/?>.<']+"></td>
            </tr>

            <tr>
                <td></td>
                <td>Nomor telepon yang dapat dihubungi. Pisahkan dengan ,
                    (koma) jika ada beberapa nomor.</td>
            </tr>
            <!-------------------------------------------------------------------->

            <tr>
                    <td></td>
                    <td style="color: red;display: none" id="hpWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>No. H.P. </td>
                <td><input type="text" id="hp" required pattern="[^a-zA-Z!@#$%^&*-+=_~`'/?>.<']+"></td>
            </tr>

            <tr>
                <td></td>
                <td>Nomor handphone. Pisahkan dengan ,
                    (koma) jika ada beberapa nomor.</td>
            </tr>
            <!-------------------------------------------------------------------->

            <tr>
                <td></td>
                <td style="color: red;display: none" id="emailWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Email * </td>
                <td><input type="email" id="email" pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{1,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"></td>
            </tr>

            <tr>
                <td></td>
                <td>Pastikan alamat email Anda benar,
                    email konfirmasi akan dikirim ke alamat email tersebut</td>
            </tr>
            <!-------------------------------------------------------------------->
            <tr>
                <td></td>
                <td style="color: red;display: none" id="reemailWarning1">Field ini harus diisi</td>
            </tr>

            <tr>
                <td>Ulangi Email</td>
                <td><input type="email" id="reemail" pattern="^[\w]{1,}[\w.+-]{0,}@[\w-]{1,}([.][a-zA-Z]{2,}|[.][\w-]{2,}[.][a-zA-Z]{2,})$"></td>
            </tr>
            <!------------------------------------------------------------------------------------------------->
            <!------------------- -------------------------------------------------------------------------- -->

            <tr>
                <td></td>
                <td><button type="button" onclick="cekIsi()">Simpan</button></td>
            </tr>


        </table>

    </form>

    <script type="text/javascript">

        function cekIsi() {
            var username = document.getElementById("username");
            var password = document.getElementById("password");
            var repassword = document.getElementById("repassword");
            var namez = document.getElementById("namez");
            var ijazah = document.getElementById("ijazah");
            var noIdent = document.getElementById("noIdent");
            var jk = document.getElementsByName("jk");
            var hari = document.getElementById("hari");
            var bulan = document.getElementById("bulan");
            var tahun = document.getElementById("tahun");
            var alamat1 = document.getElementById("alamat1");
            var propinsi = document.getElementById("propinsi");
            var kk = document.getElementById("kk");
            var alamat2 = document.getElementById("alamat2");
            var telp = document.getElementById("telp");
            var hp = document.getElementById("hp");
            var email = document.getElementById("email");
            var reemail = document.getElementById("reemail");





            if (username.value == "") {
                usernameWarning1.style.display = "block";
            } else {
                usernameWarning1.style.display = "none";
            }

            if (password.value != "") {
                if (password.value.length >= 6) {
                    if (password.value != repassword.value) {
                        document.getElementById("passwordWarning1").innerHTML = "Password yang Anda masukkan tidak sama";
                        passwordWarning1.style.display = "block";
                    } else {
                        passwordWarning1.style.display = "none";
                    }
                } else {
                    document.getElementById("passwordWarning1").innerHTML = "Password minimal terdiri dari 6 karakter";
                    passwordWarning1.style.display = "block";
                }
            } else {
                document.getElementById("passwordWarning1").innerHTML = "Field ini harus diisi";
                passwordWarning1.style.display = "block";
            }

            if (repassword.value != "") {
                repasswordWarning1.style.display = "none";

            } else {
                repasswordWarning1.style.display = "block";
            }

            if (password.value == "") {
                repassword.value = "";
            } else if (repassword.value == "") {
                password.value = "";
            } else if (password.value == repassword.value && password.value.length <= 125) {
                password.value = "";
                repassword.value = "";
            } else if (password.value != repassword.value && password.value.length <= 125) {
                password.value = "";
                repassword.value = "";
            }

            if (namez.value == "") {
                namezWarning1.style.display = "block";
            } else {
                namezWarning1.style.display = "none";
            }

            if (ijazah.value == "") {
                ijazahWarning1.style.display = "block";
            } else {
                ijazahWarning1.style.display = "none";
            }

            if (ident.value == "") {
                identWarning1.style.display = "block";
            } else {
                identWarning1.style.display = "none";
            }

            if (noIdent.value == "") {
                noIdentWarning1.style.display = "block";
            } else {
                noIdentWarning1.style.display = "none";
            }

            if (jk[0].checked || jk[1].checked) {
                // for (var i = 0; i < jk.length; i++) {
                //     if (jk[i].checked) {
                //         jkWarning1.style.display = "none";
                //     } else {
                //         jkWarning1.style.display = "block";
                //     }
                // }
                jkWarning1.style.display = "none";
            } else {
                jkWarning1.style.display = "block";
            }

            if (hari.value == "" || bulan.value == "" || tahun.value == "") {
                tglWarning1.style.display = "block";
            } else {
                tglWarning1.style.display = "none";
            }

            if (alamat1.value == "") {
                alamat1Warning1.style.display = "block";
            } else {
                alamat1Warning1.style.display = "none";
            }

            // for (var i = 1; i <= negara.length; i++) {
            //     if (negara[i].selected) {
            //         negaraWarning1.style.display = "none";
            //     } else if (negara[0].selected) {
            //         negaraWarning1.style.display = "block";
            //     }

            // }

            if(negara[0].selected){
                negaraWarning1.style.display = "block";
            }else{
                negaraWarning1.style.display = "none";
            }

            if (propinsi[0].selected) {
                 propinsiWarning1.style.display = "block";
            }else{
                propinsiWarning1.style.display = "none";
            }

            if (kk[0].selected) {
                 kkWarning1.style.display = "block";
            }else{
                kkWarning1.style.display = "none";
            }

            // for (var i = 1; i <= propinsi.length; i++) {
            //     if (propinsi[i].selected) {
            //         propinsiWarning1.style.display = "none";
            //     } else if (propinsi[0].selected) {
            //         propinsiWarning1.style.display = "block";
            //     }

            //}

                
            if (alamat2.value == "") {
                alamat2Warning1.style.display = "block";
            } else {
                alamat2Warning1.style.display = "none";
            }
            
            if (telp.value == "") {
                    telpWarning1.style.display = "block";
            } else {
                    telpWarning1.style.display = "none";
            }

            if (hp.value == "") {
                    hpWarning1.style.display = "block";
            } else {
                    hpWarning1.style.display = "none";
            }

            if (email.value != "") {
                if (email.value != reemail.value) {
                    document.getElementById("emailWarning1").innerHTML = "Email yang Anda masukkan tidak sama";
                    emailWarning1.style.display = "block";
                } else {
                    emailWarning1.style.display = "none";
                }
            } else {
                document.getElementById("emailWarning1").innerHTML = "Field ini harus diisi";
                emailWarning1.style.display = "block";
            }

            if (reemail.value != "") {
                reemailWarning1.style.display = "none";

            } else {
                reemailWarning1.style.display = "block";
            }
 
        }

        function pilihPropinsi() {
            var propinsi = document.getElementById("propinsi");
            var negara = document.getElementById("negara");
            var option1 = document.createElement("option");
            var option2 = document.createElement("option");
            var option3 = document.createElement("option");
            var option4 = document.createElement("option");


            if (negara.value == "indonesia") {

                propinsi.remove(option1);
                propinsi.remove(option2);
                propinsi.remove(option3);
                propinsi.remove(option4);

                option1.text = "-- Pilih --";
                option2.text = "Banten";
                option3.text = "DKI Jakarta";
                option4.text = "Jawa Barat";


                option1.value = "";
                option2.value = "banten";
                option3.value = "jakarta";
                option4.value = "jawa_barat";

                propinsi.add(option1);
                propinsi.add(option2);
                propinsi.add(option3);
                propinsi.add(option4);

            } else if (negara.value == "india") {

                propinsi.remove(option1);
                propinsi.remove(option2);
                propinsi.remove(option3);
                propinsi.remove(option4);

                option1.text = "-- Pilih --";
                option2.text = "Lain-lain";

                option1.value = "";
                option2.value = "lain-lain";

                propinsi.add(option1);
                propinsi.add(option2);

            } else if (negara.value == "united_kingdom") {

                propinsi.remove(option1);
                propinsi.remove(option2);
                propinsi.remove(option3);
                propinsi.remove(option4);

                option1.text = "-- Pilih --";
                option2.text = "Lain-lain";

                option1.value = "";
                option2.value = "lain-lain";

                propinsi.add(option1);
                propinsi.add(option2);
            } else {
                propinsi.remove(option1);
                propinsi.remove(option2);
                propinsi.remove(option3);
                propinsi.remove(option4);

                option1.text = "-- Pilih --";

                option1.value = "";

                propinsi.add(option1);

            }

        }

        function pilihKk() {
            var kk = document.getElementById("kk");
            var propinsi = document.getElementById("propinsi");
            var option1 = document.createElement("option");
            var option2 = document.createElement("option");
            var option3 = document.createElement("option");
            var option4 = document.createElement("option");
            var option5 = document.createElement("option");
            var option6 = document.createElement("option");
            var option7 = document.createElement("option");
            var option8 = document.createElement("option");
            var option9 = document.createElement("option");
            var option10 = document.createElement("option");

            if (propinsi.value == "banten") {
                kk.remove(option1);
                kk.remove(option2);
                kk.remove(option3);
                kk.remove(option4);
                kk.remove(option5);
                kk.remove(option6);
                kk.remove(option7);
                kk.remove(option8);
                kk.remove(option9);
                kk.remove(option10);

                option1.text = "-- Pilih --";
                option2.text = "Kab. lebak";
                option3.text = "Kab. Pandeglang";
                option4.text = "Kab. Serang";
                option5.text = "Kab. Tanggerang";
                option6.text = "Kota Cilegon";
                option7.text = "Kota Serang";
                option8.text = "Kota Tanggerang";
                option9.text = "Kota Tanggerang Selatan";
                option10.text = "Lain-lain";

                option1.value = "";
                option2.value = "kab_lebak";
                option3.value = "kab_pandeglang";
                option4.value = "kab_serang";
                option5.value = "kab_tanggerang";
                option6.value = "kota_cilegon";
                option7.value = "kota_serang";
                option8.value = "kota_tanggerang";
                option9.value = "kota_tanggerang_selatan";
                option10.value = "lain-lain";

                kk.add(option1);
                kk.add(option2);
                kk.add(option3);
                kk.add(option4);
                kk.add(option5);
                kk.add(option6);
                kk.add(option7);
                kk.add(option8);
                kk.add(option9);
                kk.add(option10);
            }

            else if (propinsi.value == "jakarta") {
                kk.remove(option1);
                kk.remove(option2);
                kk.remove(option3);
                kk.remove(option4);
                kk.remove(option5);
                kk.remove(option6);
                kk.remove(option7);
                kk.remove(option8);
                kk.remove(option9);
                kk.remove(option10);

                option1.text = "-- Pilih --";
                option2.text = "Kab. Kepulauan Seribu";
                option3.text = "Kota Jakarta Barat";
                option4.text = "Kota Jakarta Pusat";
                option5.text = "Kota Jakarta Selatan";
                option6.text = "Kota Jakarta Timur";
                option7.text = "Kota Jakarta Utara";
                option8.text = "Lain-lain";

                option1.value = "";
                option2.value = "kab_kepulauan_seribu";
                option3.value = "kota_jakarta_barat";
                option4.value = "kota_jakarta_pusat";
                option5.value = "kota_jakarta_selatan";
                option6.value = "kota_jakarta_timur";
                option7.value = "kota_jakarta_utara";
                option8.value = "lain-lain";

                kk.add(option1);
                kk.add(option2);
                kk.add(option3);
                kk.add(option4);
                kk.add(option5);
                kk.add(option6);
                kk.add(option7);
                kk.add(option8);
            }
            else if (propinsi.value == "jawa_barat") {
                kk.remove(option1);
                kk.remove(option2);
                kk.remove(option3);
                kk.remove(option4);
                kk.remove(option5);
                kk.remove(option6);
                kk.remove(option7);
                kk.remove(option8);
                kk.remove(option9);
                kk.remove(option10);

                option1.text = "-- Pilih --";
                option2.text = "Kab. Indramayu";
                option3.text = "Kab. Karawang";
                option4.text = "Kab. Cianjur";
                option5.text = "Kota Bandung";
                option6.text = "Kota Bekasi";
                option7.text = "Kota Bogor";
                option8.text = "Kota Depok";
                option9.text = "Kota Sukabumi";
                option10.text = "Lain-lain";

                option1.value = "";
                option2.value = "kab_indramayu";
                option3.value = "kab_karawang";
                option4.value = "kab_cianjur";
                option5.value = "kota_bandung";
                option6.value = "kota_bekasi";
                option7.value = "kota_bogor";
                option8.value = "kota_depok";
                option9.value = "kota_sukabumi";
                option10.value = "lain-lain";

                kk.add(option1);
                kk.add(option2);
                kk.add(option3);
                kk.add(option4);
                kk.add(option5);
                kk.add(option6);
                kk.add(option7);
                kk.add(option8);
                kk.add(option9);
                kk.add(option10);
            }
            else if (propinsi.value == "lain-lain") {
                kk.remove(option1);
                kk.remove(option2);
                kk.remove(option3);
                kk.remove(option4);
                kk.remove(option5);
                kk.remove(option6);
                kk.remove(option7);
                kk.remove(option8);
                kk.remove(option9);
                kk.remove(option10);

                option1.text = "-- Pilih --";
                option2.text = "Lain-lain";

                option1.value = "";
                option2.value = "lain-lain";

                kk.add(option1);
                kk.add(option2);
            }
            else {
                kk.remove(option1);
                kk.remove(option2);
                kk.remove(option3);
                kk.remove(option4);
                kk.remove(option5);
                kk.remove(option6);
                kk.remove(option7);
                kk.remove(option8);
                kk.remove(option9);
                kk.remove(option10);
            }

        }


    </script>


</body>

</html>